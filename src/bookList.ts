export default interface Book{
    authors:Array<any>,
    covers:Array<any>,
    created:object,
    key:string,
    last_modified:Object, 
    latest_revision:number,
    revision:number,
    title:string
    type:object
}