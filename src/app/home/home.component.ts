import { Component } from '@angular/core';
import { BooksService } from '../books.service';
import { BookCardComponent } from "../book-card/book-card.component";

@Component({
    selector: 'app-home',
    standalone: true,
    template: `
    <section>
      <app-book-card [books]="allBooks"></app-book-card>
    </section>  
  `,
    styleUrl: './home.component.css',
    imports: [BookCardComponent]
})
export class HomeComponent {
  SKBooks = new BooksService()
  allBooks = new Array
  constructor(){
    this.SKBooks.getAllSKBooks()
      .then(books =>this.allBooks=books.entries)   
      .then(()=>console.log(this.allBooks));
      
  }
}
