import { Component, Input } from '@angular/core';
import { RouterLink } from '@angular/router';
@Component({
  selector: 'app-book-card',
  standalone: true,
  imports: [RouterLink],
  template: `
    <section id="mainBookCard">
      @for (book of books; track $index) {
      <div class="CardCont center shadow">
        @if(book.covers){
        <img
          class="bookCover shadow"
          src="{{
            'https://covers.openlibrary.org/b/id/' + book.covers[0] + '-M.jpg'
          }}"
          alt="{{ book.title }}"
          srcset=""
        />
        }@else {
        <img
          class="bookCover shadow"
          src="https://bookcart.azurewebsites.net/Upload/Default_image.jpg"
          alt="{{ book.title }}"
        />
        }
        <div>{{ book.title }}</div>

        <a class="center" [routerLink]="['descri', book.key]" >Voir plus</a>
      </div>

      }
    </section>
  `,
  styleUrl: './book-card.component.css',
})
export class BookCardComponent {
  @Input() books: any;

}
