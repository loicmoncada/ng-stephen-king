import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class BooksService {
  urlB = 'https://openlibrary.org';
  ATH_URI = 'authors';
  BOK_URI="books";
  ATH="OL2623310A";

  async getAllSKBooks():Promise<any> {
    const data: Promise<any> = await fetch(
      `${this.urlB}/${this.ATH_URI}/${this.ATH}/works.json`
    ).then((r) => r.json());
    return await data;
  }

  async getBookById(id:any):Promise<any> {
    let endpoint = `${this.urlB}${id}.json`
    console.log(endpoint);
    const data: Promise<any> = await fetch(endpoint)
      .then((r) => r.json());
        
    return await data;
  }
}
