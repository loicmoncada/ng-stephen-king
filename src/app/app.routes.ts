import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DescriComponent } from './descri/descri.component';

export const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    title: 'HomePage',
  },
  {
    path: 'descri/:id',
    component: DescriComponent,
    title: 'descri',
  }
];
