import { Component, inject } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { BooksService } from '../books.service';

@Component({
  selector: 'app-descri',
  standalone: true,
  imports: [],
  template: `
    <section>
      <div>
        <div>{{book.title}}</div>
        @if(book.covers){
        <img
          class="bookCover shadow"
          src="{{
            'https://covers.openlibrary.org/b/id/' + book.covers[0] + '-M.jpg'
          }}"
          alt="{{ book.title }}"
          srcset=""
        />
        }@else {
        <img
          class="bookCover shadow"
          src="https://bookcart.azurewebsites.net/Upload/Default_image.jpg"
          alt="{{ book.title }}"
        />
        }
        <div>Edition : {{book.created.value}}</div>
      </div>
    </section>
  `,
  styleUrl: './descri.component.css'
})
export class DescriComponent {
  book:any

  route:ActivatedRoute =inject(ActivatedRoute)
  bookService:BooksService=inject(BooksService)

  constructor(){
    console.log(this.route.snapshot.params['id']);
    this.bookService.getBookById(this.route.snapshot.params['id']).then(r=>this.book=r);
    
  }
}


