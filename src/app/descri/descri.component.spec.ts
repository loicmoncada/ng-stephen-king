import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DescriComponent } from './descri.component';

describe('DescriComponent', () => {
  let component: DescriComponent;
  let fixture: ComponentFixture<DescriComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DescriComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DescriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
